

let trainer = {
    name: "Ash ketchum",
    age: 10,
    friends: {hoenn: ["May", "Max"], kanto: ["Brock", "Misty"]},
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    talk: function(chosenPokemon){
        if (this.pokemon.includes(chosenPokemon)){
      console.log( chosenPokemon + "! I choose you!" );
    
        }
}
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:")
console.log(trainer["pokemon"])
console.log("Result of talk method");
trainer.talk("Charizard");


function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = level*2;
    this.attack = level;

    this.tackle = function(target){
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log(target.name +"'s health is now reduced to " + target.health)
        if(target.health <=0){            
          let fainted = target.name;
          this.faint(fainted);
        }
       
    }
    this.faint = function(faint){
        console.log(faint + " fainted");
    }
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let Geodude = new Pokemon("Geodude", 8);
console.log(Geodude);

let Mewtwo = new Pokemon("Mewtwo", 100);
console.log(Mewtwo);


Mewtwo.tackle(Geodude);
pikachu.tackle(Mewtwo);













